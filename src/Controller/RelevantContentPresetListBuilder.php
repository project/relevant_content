<?php

namespace Drupal\relevant_content\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a listing of RelevantContentPreset.
 */
class RelevantContentPresetListBuilder extends ConfigEntityListBuilder {

  /**
   * Used during construction to generate and cache a list of vocabulary names.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $vocabularyStorage;

  /**
   * Used to cache a mapping of node type to labels array.
   *
   * @var array|string[]
   */
  protected array $nodeTypeNamesCache;

  /**
   * Used to cache a mapping of vocabulary types to labels.
   *
   * @var array
   */
  protected array $vocabularyNamesCache;

  /**
   * Constructs a new RelevantContentPresetListBuilder class.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $vocabulary_storage
   *   The Taxonomy vocabulary storage class.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, EntityStorageInterface $vocabulary_storage) {
    parent::__construct($entity_type, $storage);
    $this->nodeTypeNamesCache = node_type_get_names();
    $this->vocabularyStorage = $vocabulary_storage;

    $this->vocabularyNamesCache = [];
    foreach ($this->vocabularyStorage->loadMultiple() as $vocabulary) {
      $this->vocabularyNamesCache[$vocabulary->id()] = $vocabulary->label();
    }
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container this object should use.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   *
   * @return \Drupal\Core\Entity\EntityListBuilder|\Drupal\relevant_content\Controller\RelevantContentPresetListBuilder|static
   *   A new instance of the RelevantContentPresetListBuilder class.
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('entity_type.manager')->getStorage('taxonomy_vocabulary')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Preset Name');
    $header['id'] = $this->t('Machine name');
    $header['node_types'] = $this->t('Node Types');
    $header['vocabularies'] = $this->t('Vocabularies');
    $header['max_results'] = $this->t('Max Results');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\relevant_content\Entity\RelevantContentPresetInterface $entity */
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['node_types'] = implode(', ', array_map(fn($type) => $this->nodeTypeNamesCache[$type], $entity->get('node_types')));
    $row['vocabularies'] = implode(', ', array_map(fn($type) => $this->vocabularyNamesCache[$type], $entity->get('vocabularies')));
    $row['max_results'] = $entity->get('max_results');

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();
    $build['table']['#empty'] = $this->t('There are no presets yet. <a href=":url">Add a new one</a>.', [
      ':url' => Url::fromRoute('entity.relevant_content_preset.add_form')->toString(),
    ]);
    return $build;
  }

}
