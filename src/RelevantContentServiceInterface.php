<?php

namespace Drupal\relevant_content;

use Drupal\node\Entity\Node;

/**
 * An Interface for the Relevant Content Service class.
 *
 * This mainly exists so we can use dependency injection.
 */
interface RelevantContentServiceInterface {

  /**
   * Get array of term ID's for the current page.
   *
   * @param \Drupal\node\Entity\Node|null $node
   *   Optional. The node to find terms for. If not provided, will try to find
   *   the node from the current route.
   * @param array $vocabs
   *   Optional. Filter for terms only in these vocabs. If empty, no filtering
   *   will be applied. For example: ['tags'].
   *
   * @return array
   *   An array of Term IDs for this node filtered by the provided vocabularies.
   */
  public function getNodeTerms(Node $node = NULL, array $vocabs = []): array;

  /**
   * Function to get a set of nodes.
   *
   * This returns a set of nodes based on the provided type and array of term
   * ID's.
   *
   * @param array $types
   *   Array representing the node types.
   * @param array $terms
   *   Array of Term ID's.
   * @param array $exclude
   *   Optional: An array of Node ID's to exclude. Useful for excluding
   *   the node you might be comparing to currently. Default: No exclusions.
   * @param int $limit
   *   Optional: Integer controlling the maximum number of nodes returned.
   *   Default: 5.
   * @param array $languages
   *   Optional: An array of languages to restrict nodes to.
   *             An empty string in the array corresponds to Language Neutral.
   *             An empty array will include all nodes regardless of language.
   *
   * @return mixed
   *   FALSE if no result or error. An associative array keyed by node ID with
   *   an array describing the node with a nid, vid, title, type & term match
   *   counts.
   */
  public function findRelevantContent(array $types, array $terms, array $exclude = [], int $limit = 5, array $languages = []): mixed;

}
