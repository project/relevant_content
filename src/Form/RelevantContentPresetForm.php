<?php

namespace Drupal\relevant_content\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for the RelevantContentPreset add and edit forms.
 */
class RelevantContentPresetForm extends EntityForm {

  /**
   * Constructs an RelevantContentPresetForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entityTypeManager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\relevant_content\Entity\RelevantContentPreset $relevant_content_preset */
    $relevant_content_preset = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $relevant_content_preset->label(),
      '#description' => $this->t("Label for the RelevantContentPreset."),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $relevant_content_preset->id(),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
      '#disabled' => !$relevant_content_preset->isNew(),
    ];

    $form['node_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled Content Types'),
      '#description' => $this->t('Only enabled content types will show in the results.'),
      '#default_value' => $relevant_content_preset->get('node_types') ?? [],
      '#options' => node_type_get_names(),
    ];

    $vocabularies = [];
    foreach ($this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple() as $vocabulary) {
      $vocabularies[$vocabulary->id()] = $vocabulary->label();
    }
    $form['vocabularies'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled Vocabularies'),
      '#description' => $this->t('Only enabled vocabularies will be searched. For example, you may only want to find related content based on "Tags".'),
      '#default_value' => $relevant_content_preset->get('vocabularies') ?? [],
      '#options' => $vocabularies,
    ];

    $form['max_results'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum results'),
      '#default_value' => $relevant_content_preset->get('max_results') ?? 5,
      '#min' => 0,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $relevant_content = $this->entity;
    $status = $relevant_content->save();

    if ($status === SAVED_NEW) {
      $this->messenger()->addMessage($this->t('The %label Preset has been created.', [
        '%label' => $relevant_content->label(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %label Preset was updated.', [
        '%label' => $relevant_content->label(),
      ]));
    }

    $form_state->setRedirect('entity.relevant_content_preset.collection');
  }

  /**
   * Helper function to check whether a Preset entity exists.
   */
  public function exist($id) {
    $entity = $this->entityTypeManager->getStorage('relevant_content_preset')->getQuery()
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

}
