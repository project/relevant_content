<?php

namespace Drupal\relevant_content\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the RelevantContentPreset entity.
 *
 * @ConfigEntityType(
 *   id = "relevant_content_preset",
 *   label = @Translation("Relevant Content Preset"),
 *   handlers = {
 *     "list_builder" = "Drupal\relevant_content\Controller\RelevantContentPresetListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "form" = {
 *       "add" = "Drupal\relevant_content\Form\RelevantContentPresetForm",
 *       "edit" = "Drupal\relevant_content\Form\RelevantContentPresetForm",
 *       "delete" = "Drupal\relevant_content\Form\RelevantContentPresetDeleteForm",
 *     }
 *   },
 *   config_prefix = "relevant_content",
 *   admin_permission = "administer relevant content",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "node_types",
 *     "vocabularies",
 *     "max_results"
 *   },
 *   links = {
 *     "collection" = "/admin/config/search/relevant-content",
 *     "add-form" = "/admin/config/search/relevant-content/add",
 *     "edit-form" = "/admin/config/search/relevant-content/{relevant_content_preset}",
 *     "delete-form" = "/admin/config/search/relevant-content/{relevant_content_preset}/delete"
 *   }
 * )
 */
class RelevantContentPreset extends ConfigEntityBase implements RelevantContentPresetInterface {

  /**
   * A machine name for the config item.
   *
   * @var string
   */
  protected string $id;

  /**
   * A label for the item. Used as a title of the block by default.
   *
   * @var string
   */
  protected string $label;

  /**
   * Array of node types to filter by.
   *
   * @var array
   */
  protected array $node_types;

  /**
   * Array of vocabularies to filter by.
   *
   * @var array
   */
  protected array $vocabularies;

  /**
   * Maximum number of results to show.
   *
   * @var int
   */
  protected int $max_results;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    // Filter the arrays to remove empty/zero-value items.
    $this->set('node_types', array_filter($this->get('node_types')));
    $this->set('vocabularies', array_filter($this->get('vocabularies')));
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    $return = parent::save();
    // Invalidate the block cache to update the block derivatives.
    if (\Drupal::moduleHandler()->moduleExists('block')) {
      \Drupal::service('plugin.manager.block')->clearCachedDefinitions();
    }
    return $return;
  }

}
