<?php

namespace Drupal\relevant_content\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a Relevant Content Preset entity.
 */
interface RelevantContentPresetInterface extends ConfigEntityInterface {}
