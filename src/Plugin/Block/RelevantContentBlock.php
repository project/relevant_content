<?php

namespace Drupal\relevant_content\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\relevant_content\Entity\RelevantContentPreset;
use Drupal\relevant_content\RelevantContentServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block of relevant content for the current page.
 *
 * @Block(
 *   id = "relevant_content_block",
 *   admin_label = @Translation("Relevant Content"),
 *   category = @Translation("Relevant Content"),
 *   deriver = "Drupal\relevant_content\Plugin\Derivative\RelevantContentBlock",
 *   context_definitions = {
 *     "node" = @ContextDefinition("entity:node", label = @Translation("Node"))
 *   }
 * )
 */
class RelevantContentBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The Relevant Content Service class.
   *
   * @var \Drupal\relevant_content\RelevantContentServiceInterface
   */
  protected RelevantContentServiceInterface $relevantContentService;

  /**
   * Creates a new RelevantContentBlock class.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RelevantContentServiceInterface $relevant_content_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->relevantContentService = $relevant_content_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('relevant_content')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $preset = RelevantContentPreset::load($this->getDerivativeId());

    /** @var \Drupal\node\Entity\Node $node */
    $node = $this->getContextValue('node');

    $terms = $this->relevantContentService->getNodeTerms($node, $preset->get('vocabularies'));

    $related = $this->relevantContentService->findRelevantContent(
      $preset->get('node_types'),
      $terms,
      [$node->id()],
      $preset->get('max_results')
    );

    return [
      '#theme' => 'relevant_content_results',
      '#nodes' => $related,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $cache_tags = parent::getCacheTags();
    $cache_tags[] = 'config:relevant_content.relevant_content.' . $this->getDerivativeId();
    return $cache_tags;
  }

}
