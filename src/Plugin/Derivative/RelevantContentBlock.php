<?php

namespace Drupal\relevant_content\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 */
class RelevantContentBlock extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The Relevant Content Preset storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $presetStorage;

  /**
   *
   */
  public function __construct(EntityStorageInterface $preset_storage) {
    $this->presetStorage = $preset_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager')->getStorage('relevant_content_preset')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach ($this->presetStorage->loadMultiple() as $preset => $entity) {
      $this->derivatives[$preset] = $base_plugin_definition;
      $this->derivatives[$preset]['admin_label'] = $entity->label();
      $this->derivatives[$preset]['config_dependencies']['config'] = [$entity->getConfigDependencyName()];
    }
    return $this->derivatives;
  }

}
