<?php

namespace Drupal\relevant_content\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\node\Entity\Node;

/**
 * The TermAlter class is an Event used to let other alter terms for search.
 */
class TermAlter extends Event {

  const ALTER = 'relevnt_content.term_alter';

  /**
   * Array of terms for the Relevant Content load.
   *
   * @var array
   */
  protected array $terms;

  /**
   * The node that triggered this Relevant Content lookup.
   *
   * @var \Drupal\node\Entity\Node
   */
  protected Node $node;

  /**
   * Constructs a new TermAlter Event.
   *
   * @param array $terms
   *   An array of terms to alter.
   * @param \Drupal\node\Entity\Node $node
   *   The node that these were loaded for, only for reference not for altering!
   */
  public function __construct(array $terms, Node $node) {
    $this->terms = $terms;
    $this->node = $node;
  }

  /**
   * Getter for $node.
   *
   * @return \Drupal\node\Entity\Node
   *   The node the terms are associated with. Not for altering.
   */
  public function getNode(): Node {
    return $this->node;
  }

  /**
   * Getter for $terms.
   *
   * @return array
   *   An array of term IDs that can be altered using the setter.
   */
  public function getTerms(): array {
    return $this->terms;
  }

  /**
   * Setter for $terms.
   *
   * @param array $terms
   *   Set the terms. Essentially allowing altering.
   */
  public function setTerms(array $terms): void {
    $this->terms = $terms;
  }

}
