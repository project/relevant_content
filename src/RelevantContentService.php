<?php

namespace Drupal\relevant_content;

use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\relevant_content\Event\TermAlter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Relevant Content Service class for some helper methods.
 *
 * This service class is mainly a wrapper around two handy methods. One for
 * getting terms associated with a node. Another for lookup up other nodes that
 * are "relevant" to the one supplies (taking into account constraints)
 */
class RelevantContentService implements RelevantContentServiceInterface {
  /**
   * The service container.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
  protected ContainerInterface $container;

  /**
   * Reference to the database container.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $db;

  /**
   * The Event Dispatcher class.
   *
   * Used to invoke the TermAlter class.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * Creates a Relevant Content Service class.
   */
  public function __construct(ContainerInterface $container, EventDispatcherInterface $event_dispatcher) {
    $this->container = $container;
    $this->eventDispatcher = $event_dispatcher;

    $this->db = $container->get('database');
  }

  /**
   * {@inheritdoc}
   */
  public function getNodeTerms(Node $node = NULL, array $vocabs = []): array {
    // If we have passed in a node, or if there is one available from the route,
    // use the node ID to load all the terms for the node. This is necessary as
    // terms can ger references by multiple different fields.
    if (isset($node) || ($node = \Drupal::routeMatch()->getParameter('node'))) {
      // Select from the taxonomy_index and the taxonomy_term_data.
      $query = $this->db->select('taxonomy_index', 't');

      // We need the Term ID, Name and Vocabulary ID.
      $query->addField('t', 'tid');

      // And we need to filter for the node in question.
      $query->condition('t.nid', $node->id());

      if (!empty($vocabs)) {
        $query->join('taxonomy_term_data', 'td', 'td.tid = t.tid');
        $query->condition('td.vid', $vocabs, 'IN');
      }

      // Get the terms as a tid-indexed array of objects.
      $terms = $query->execute()->fetchCol();

      // Provide a hook_relevant_content_terms_alter so that other modules can
      // change or add to the relevant terms.
      $this->eventDispatcher->dispatch(new TermAlter($terms, $node), TermAlter::ALTER);

      return $terms;
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function findRelevantContent(array $types, array $terms, array $exclude = [], int $limit = 5, array $languages = []): mixed {
    // If terms or types are empty, there isn't anything to match to so not a
    // lot of point continuing.
    if (empty($terms) || empty($types)) {
      return FALSE;
    }

    $query = $this->db->select('node_field_data', 'n');
    $query->join('taxonomy_index', 'ti', 'n.nid = ti.nid');
    $query->addField('n', 'nid');
    $query->addExpression('COUNT(*)', 'cnt');

    // Filter for all nodes which are published, in the "types" array and has at
    // least one of the selected terms.
    $query->condition('n.status', NodeInterface::PUBLISHED)
      ->condition('n.type', $types, 'IN')
      ->condition('ti.tid', $terms, 'IN');

    // Exclude any specific node id's.
    if (!empty($exclude)) {
      $query->condition('n.nid', $exclude, 'NOT IN');
    }

    // IF language is specified, make sure we filter for those too.
    if (!empty($languages)) {
      $query->condition('n.language', $languages, 'IN');
    }

    // Group, order and limit the query.
    $query->groupBy('n.nid')
      ->orderBy('cnt', 'DESC')
      ->orderBy('n.created', 'DESC')
      ->orderBy('n.nid', 'DESC')
      ->range(0, $limit);

    // Execute and loop to store the results against the node ID key.
    $nodes = $query->execute()->fetchAllAssoc('nid', \PDO::FETCH_ASSOC);

    // Bail here if no results.
    if (empty($nodes)) {
      return FALSE;
    }

    // Preload these nodes into the array.
    $entities = Node::loadMultiple(array_keys($nodes));
    foreach ($nodes as $nid => &$result) {
      $result['entity'] = $entities[$nid];
    }

    return $nodes;
  }

}
